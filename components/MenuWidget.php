<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/27/2018
 * Time: 17:07
 */

namespace app\components;
use yii\base\Widget;
use app\models\Category;
use Yii;

class MenuWidget extends Widget
{
    public $frontend;
    public $model;
    public $data; // масив категорій з бд
    public $tree; // будуємо дерево з масива
    public $menuHtml; // HTML код, залежить від $frontend

    public function init()
    {
        parent::init();
        if ($this->frontend === null) {
            $this->frontend = 'menu';
        }
        $this->frontend .= '.php';
    }

    public function run()
    {
       /* $menu = \Yii::$app->cache->get('menu');
        if ($menu) {
            return $menu;
        }*/

        $this->data = Category::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);

        //Yii::$app->cache->set('menu',$this->menuHtml, 60);

        return $this->menuHtml;
    }

    public function getTree()
    {
        $tree = [];
        foreach ($this->data as $id => &$node) {
            if (!$node['parent_id']){
                $tree[$id] = &$node;
            }
             else{
                 $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
             }
        }
        return $tree;
    }

    public function getMenuHtml($tree)
    {
        $str = '';
        foreach ($tree as $category) {
            $str .= $this->catToTemplate($category);

        }
        return $str;
    }

    public function catToTemplate($category)
    {
        ob_start();
        include __DIR__ . '/menu_frontend/' . $this->frontend;
        return ob_get_clean();
    }


}