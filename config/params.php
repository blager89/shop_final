<?php

return [
    'adminEmail' => 'admin@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'supportEmail' => 'sashanevirkovets@gmail.com'
];
