<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/30/2018
 * Time: 21:24
 */

namespace app\controllers;


use yii\web\Controller;
use app\models\Cart;
use app\models\Product;
use Yii;
use app\models\OrderItems;
use app\models\Order;

class CartController extends Controller
{
    public function actionAdd()
    {
        $id = Yii::$app->request->get('id');
        $qty = (int)Yii::$app->request->get('qty');
        $itemCount = !$qty ? 1 : $qty;
        $product = Product::findOne($id);
        if (empty($product)) return false;
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product,$itemCount);

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        $this->layout = false;
        return $this->render('cart', compact('session'));
    }

    public function actionClear()
    {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.count');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart', compact('session'));

    }

    public function actionDellItem()
    {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalculation($id);

        $this->layout = false;
        return $this->render('cart', compact('session'));
    }

    public function actionShowCart()
    {
        $session = Yii::$app->session;
        $session->open();

        $this->layout = false;
        return $this->render('cart', compact('session'));
    }

    public function actionView()
    {
        $session = Yii::$app->session;
        $session->open();
        $order = new Order();
        if ($order->load(Yii::$app->request->post())) {
            $order->qty = $session['cart.count'];
            $order->sum = $session['cart.sum'];
            if ($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Your order is accepted');
                //from user
                Yii::$app->mailer->compose('order', ['session' => $session])
                    ->setFrom(['sashanevirkovets@gmail.com'=> 'Shop manager'])
                    ->setTo($order->email)
                    ->setSubject('Order')
                    ->send();
                //from admin
                /*Yii::$app->mailer->compose('order', ['session' => $session])
                    ->setFrom(['sashanevirkovets@gmail.com'=> 'Shop manager'])
                    ->setTo(Yii::$app->params['adminEmail'])
                    ->setSubject('Order')
                    ->send();*/



                $session->remove('cart');
                $session->remove('cart.count');
                $session->remove('cart.sum');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','error');
            }

        }
        return $this->render('view',compact('session','order'));
    }

    protected function saveOrderItems($items,$order_id)
    {
        foreach ($items as $id => $item) {
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['count'];
            $order_items->sum_item = $item['count'] * $item['price'];
            $order_items->save();


        }
    }

}