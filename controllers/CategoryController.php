<?php

namespace app\controllers;

use yii\data\Pagination;
use yii\web\Controller;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\web\HttpException;

class CategoryController extends Controller
{
    public function actionIndex()
    {
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();

        $this->setMetatag('SHOP');
        return $this->render('index',[
            'hits'=>$hits
        ]);
    }

    public function actionView($id)
    {
//        $products = Product::find()->where(['category_id' => $id])->all();
        $category = Category::findOne($id);
        if (empty($category)) {
            throw new HttpException(404,'The requested Item could not be found');
        }

        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination([
            'totalCount' => $query->count(),
            'forcePageParam' => false,
            'pageSizeParam' => false,
            'pageSize' => 3,

        ]);

        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        $this->setMetatag('Shop' . ' ' . $category->name,$category->keywords,$category->description);

        return $this->render('view', [
            'products' => $products,
            'category' => $category,
            'pages' => $pages,


        ]);
    }

    public function actionSearch()
    {
        $search = trim(Yii::$app->request->get('search'));
        if (!$search) {
            return $this->render('search');
        }
        $query = Product::find()->where(['like', 'name',$search]);
        $pages = new Pagination([
            'totalCount' => $query->count(),
            'forcePageParam' => false,
            'pageSizeParam' => false,
            'pageSize' => 3,

        ]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('search', [
            'products' => $products,
            'pages' => $pages,
            'search' => $search,

        ]);

    }

    protected function setMetatag($title = null, $keywords = null, $description = null)
    {
        $this->view -> title = $title;
        $this->view ->registerMetaTag([
            'name'=>'keywords', 'content' => "$keywords",

        ]);
        $this->view ->registerMetaTag([
            'name'=>'description', 'content' => "$description",

        ]);
    }

}