<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/30/2018
 * Time: 10:26
 */

namespace app\controllers;

use yii\data\ActiveDataProvider;

use app\models\CommentForm;
use app\models\Product;
use app\models\User;
use app\modules\admin\models\Comment;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;

class ProductController extends Controller
{
    public function actionView($id)
    {
        $product = Product::findOne($id);
        //$comments = Comment::find()->with('comments')->where(['product_id' => $id])->all();
        $commentForm = new CommentForm();



        $query = Comment::find()->with('comments')->where(['product_id' => $id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 5]);
        $comments = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();




        if (empty($product)) {
            throw new HttpException(404,'The requested Item could not be found');
        }
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();


        $dataProvider = new ActiveDataProvider([
            'query' => Comment::find()->where(['product_id'=>$id])->orderBy('date DESC'),
            'pagination' => [
                'pageSize' => 5,

            ],
        ]);



        return $this->render('view',[
            'product' => $product,
            'hits' => $hits,
            'comments' => $comments,
            'commentForm' => $commentForm,
            'pages' => $pages,
            'dataProvider' => $dataProvider,



        ]);

    }


}