<?php use yii\helpers\Html;

?>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>img</th>
                <th>name</th>
                <th>count</th>
                <th>price</th>
                <th>Sum</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($session['cart'] as $id => $item) : ?>
                <tr>
                    <td><?= Html::img('@web/images/products/'. $item['img'],['alt'=> $item['name'],'height' => 100] )?></td>
                    <td><?= $item['name'] ?></td>
                    <td><?= $item['count'] ?></td>
                    <td><?= $item['price'] ?></td>
                    <td><?= $item['count'] * $item['price']?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="3">Count</td>
                <td><?= $session['cart.count'] ?></td>
            </tr>
            <tr>
                <td colspan="3">Sum</td>
                <td><?= $session['cart.sum'] ?></td>
            </tr>
            </tbody>
        </table>
    </div>



