<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/30/2018
 * Time: 21:27
 */

namespace app\models;


use yii\base\Model;
use yii\db\ActiveRecord;

class Cart extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    public function addToCart($product, $count = 1)
    {

        $mainImg = $product->getImage();
        if (isset($_SESSION['cart'][$product->id])) {
            $_SESSION['cart'][$product->id]['count'] += $count;

        }else{
            $_SESSION['cart'][$product->id] = [
                'count' => $count,
                'name' => $product->name,
                'price' => $product->price,
                'img' => $mainImg->getUrl('x70'),


            ];
        }

        $_SESSION['cart.count'] = isset($_SESSION['cart.count']) ? $_SESSION['cart.count'] + $count : $count;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum']  + $count *  $product->price : $count * $product->price;
    }

    public function recalculation($id)
    {
        if (!isset($_SESSION['cart'][$id])) return false;
        $newCount = $_SESSION['cart'][$id]['count'];
        $newSum = $_SESSION['cart'][$id]['count'] * $_SESSION['cart'][$id]['price'];

        $_SESSION['cart.count'] -= $newCount;
        $_SESSION['cart.sum'] -= $newSum;

        unset($_SESSION['cart'][$id]);
    }


}