<?php

namespace app\models;


use app\modules\admin\models\Comment;

use yii\base\Model;

class CommentForm extends Model
{
    public $comment;

    public function  rules()
    {
        return[
            [['comment'],'required'],
            [['comment'],'string','length' => [3,250]]
        ];
    }


    public function saveComment($id)
    {
        $comment = new Comment;
        $comment->text = $this->comment;
        $comment->user_id = \Yii::$app->user->id;
        $comment->product_id = $id;
        $comment->date = date('Y-m-d H:i:s');
        return $comment->save();
    }

}


