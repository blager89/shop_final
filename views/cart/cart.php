
<?php use yii\helpers\Html;

if (!empty($session['cart'])) :?>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>img</th>
                    <th>name</th>
                    <th>count</th>
                    <th>price</th>
                    <th>delete</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['cart'] as $id => $item) : ?>
                    <tr>
                        <td class="text-center"><?= Html::img($item['img'],['alt'=> $item['name'],'height' => 50] )?></td>
                        <td><?= $item['name'] ?></td>
                        <td><?= $item['count'] ?></td>
                        <td><?= $item['price'] ?></td>
                        <td><button class="btn btn-danger dell-item" data-id="<?= $id ?>">Delete</button></td>
                    </tr>
                <?php endforeach; ?>
                    <tr>
                        <td colspan="4">Count</td>
                        <td><?= $session['cart.count'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Sum</td>
                        <td><?= $session['cart.sum'] ?></td>
                    </tr>
            </tbody>
        </table>
    </div>

<?php else: ?>
    <h3>Cart is empty </h3>
<?php endif; ?>

