<?php

$this->title = 'Cart';
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="container">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?= Yii::$app->session->getFlash('success'); ?>
    </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')) :?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>
   <? if (!empty($session['cart'])) :?>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>img</th>
                    <th>name</th>
                    <th>count</th>
                    <th>price</th>
                    <th>sum</th>
                    <th>delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($session['cart'] as $id => $item) : ?>
                    <tr>
                        <td class="text-center"><?= Html::img($item['img'],['alt'=> $item['name'],'height' => 100] )?></td>
                        <td><a href="<?= Url::to(['product/view','id'=>$id]) ?>"><?= $item['name'] ?></a></td>
                        <td><?= $item['count'] ?></td>
                        <td><?= $item['price'] ?></td>
                        <td><?= $item['count']*$item['price'] ?></td>
                        <td><button class="btn btn-danger dell-item" data-id="<?= $id ?>">Delete</button></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5">Count</td>
                    <td><?= $session['cart.count'] ?></td>
                </tr>
                <tr>
                    <td colspan="5">Sum</td>
                    <td><?= $session['cart.sum'] ?></td>
                </tr>
                </tbody>
            </table>
        </div>
       <hr>
       <?php $form = ActiveForm::begin() ?>
            <?= $form->field($order, 'name') ?>
            <?= $form->field($order, 'email') ?>
            <?= $form->field($order, 'phone') ?>
            <?= $form->field($order, 'address') ?>
            <?= Html::submitButton('odrder',['class'=>'btn btn-success ']) ?>
       <?php ActiveForm::end() ?>
    <?php else: ?>
        <h3>Cart is empty </h3>
    <?php endif; ?>


</div>
